# This is not a solution, Case #4 is supposed to be impossible
def solve_one(arr, indexes)
  sums = []

  puts '*' + indexes.inspect + '*'
  puts arr[0..(indexes.first - 1)].inspect
  sums << arr[0..(indexes.first - 1)].reduce(:+)
  ref = sums.last
  0.upto(indexes.size - 2) do |i|
    puts arr[(indexes[i]..(indexes[i + 1] - 1))].inspect
    sums << arr[(indexes[i]..(indexes[i + 1] - 1))].reduce(:+)
    return false if ref != sums.last
  end
  puts arr[indexes.last..(arr.size - 1)].inspect
  sums << arr[indexes.last..(arr.size - 1)].reduce(:+)

  return false if ref != sums.last

  true
end

def solve(arr, cuts)
  (1..(arr.size - 1)).to_a.combination(cuts).to_a.each do |indexes|
    return true if solve_one(arr, indexes)
  end

  false
end

t = gets.to_i

1.upto(t) do |i|
  r, c, h, v = gets.split(' ').map(&:to_i)

  cookie = Array.new(r) { Array.new(c) }
  row_sums = Array.new(r, 0)
  col_sums = Array.new(c, 0)
  0.upto(r - 1) do |ii|
    gets.chomp.split('').each_with_index do |char, index|
      cookie[ii][index] = char == '@' ? 1 : 0
      row_sums[ii] += cookie[ii][index]
      col_sums[index] += cookie[ii][index]
    end
  end

  solution = solve(row_sums, h) && solve(col_sums, v)
  puts "Case ##{i}: #{solution ? 'POSSIBLE' : 'IMPOSSIBLE'}"
end
