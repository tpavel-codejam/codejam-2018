def damage(program)
  beam_strength = 1
  damage_dealt = 0

  program.each do |instruction|
    if instruction == 'S'
      damage_dealt += beam_strength
    else
      beam_strength *= 2
    end
  end

  damage_dealt
end

def alter_program(old_program)
  program = old_program.clone
  str = program.join
  index = str.rindex(/CS/)

  return program unless index

  program[index] = 'S'
  program[index + 1] = 'C'

  program
end

def solve(shield, program)
  hack_count = 0

  while damage(program) > shield
    new_program = alter_program(program)

    return 'IMPOSSIBLE' if new_program == program

    program = new_program
    hack_count += 1
  end

  hack_count
end

t = gets.to_i

1.upto(t) do |i|
  values = gets.split(' ')
  shield = values.first.to_i
  program = values.last.split('')

  puts "Case ##{i}: #{solve(shield, program)}"
end
