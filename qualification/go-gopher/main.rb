$stdout.sync = true

def square_complete?(m, i, j)
  (i - 1).upto(i + 1) do |ii|
    (j - 1).upto(j + 1) do |jj|
      return false unless m[ii][jj]
    end
  end

  true
end

def solve(_a)
  garden = Array.new(1000) { Array.new(1000) }
  i = 2
  j = 2

  loop do
    j += 3 if square_complete?(garden, i, j)
    puts "#{i} #{j}"
    $stdout.flush
    ii, jj = STDIN.gets.split.map(&:to_i)
    garden[ii][jj] = 1

    return if ii.zero? && jj.zero?
    abort('mistakes were made') if ii == -1 && jj == -1
  end
end

t = STDIN.gets.chomp.to_i

1.upto(t) do
  a = STDIN.gets.chomp.to_i
  solve(a)
end
