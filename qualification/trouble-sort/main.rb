def trouble_sort(arr)
  even_part = []
  odd_part = []

  arr.each_with_index do |number, index|
    if index.even?
      even_part << number
    else
      odd_part << number
    end
  end

  even_part.sort.zip(odd_part.sort).flatten.compact
end

def solve(arr)
  trouble_sorted = trouble_sort(arr)

  0.upto(trouble_sorted.size - 2) do |i|
    return i if trouble_sorted[i] > trouble_sorted[i + 1]
  end

  'OK'
end

t = gets.to_i

1.upto(t) do |i|
  gets # N
  arr = gets.split(' ').map(&:to_i)

  puts "Case ##{i}: #{solve(arr)}"
end
